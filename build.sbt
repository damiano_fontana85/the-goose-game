
scalaVersion := "2.12.3"

name := "goose-game"
organization := "com.damiano.fontana"
version := "1.0"


libraryDependencies += "junit" % "junit" % "4.10" % Test

// for funsets
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test"
libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.4"






