# The Goose Game Kata

## Compile & Package

```
sbt assembly
```

## Run

```
java -jar target/scala-2.12/goose-game-assembly-1.0.jar 
```
