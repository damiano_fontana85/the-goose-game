//  Copyright (c) 2018 Damiano Fontana
//
//  Permission is hereby granted, free of charge, to any person
//  obtaining a copy of this software and associated documentation
//  files (the "Software"), to deal in the Software without
//  restriction, including without limitation the rights to use,
//  copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following
//  conditions:
//  
//  The above copyright notice and this permission notice shall be
//  included in all copies or substantial portions of the Software.
//  
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//  OTHER DEALINGS IN THE SOFTWARE.


package goosegame

trait Track {
  def getSpace(position : Int) : Space
  def getLastSpace : End
  def getStart: Start
  def contains(space: Space) : Boolean
}

object ClassicTrack {
  
  def apply(name: String): Track = {
    name match {
      case "classic" => new ClassicTrack(getClassicTrack())
    }
  }
  
  private def getClassicTrack() : Vector[Space] = {
		val trackNum = (0 to 63 toList)
		trackNum.map( x => fromIntToSpace(x) ).to[Vector]
  
	}
	private def fromIntToSpace(number: Int) : Space = {
		number match {
			case 0 => Start()
			case 6 => TheBridge(6,6)
			case 5 | 9 | 14 | 18 | 23 | 27 => TheGoose(number)
			case 63 => End(63)
			case y if 1 to 63 contains y => Regular(y)
		}
	}
	
	private class ClassicTrack(track: Vector[Space]) extends Track{
	  override def getSpace(position : Int) = track(position)
	  override def getLastSpace = track.last.asInstanceOf[End]
	  override def getStart = track(0).asInstanceOf[Start]
	  override def contains(space: Space) = track.contains(space)
	}
}


trait Space {
	def position: Int
	override def toString() = ""+position
	
}

case class Start() extends Space{
	override def position() = 0
	override def toString() = "Start"
	
}

case class Regular(position: Int) extends Space{
}

case class TheGoose(position: Int) extends Space{
	override def toString() = "The Goose"

}

case class TheBridge(position: Int, val bridgeJump: Int) extends Space{
		override def toString() = "The Bridge"
	
}

case class End(position: Int) extends Space{
}

case class Player(val name: String){
}