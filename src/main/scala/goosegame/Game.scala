//  Copyright (c) 2018 Damiano Fontana
//
//  Permission is hereby granted, free of charge, to any person
//  obtaining a copy of this software and associated documentation
//  files (the "Software"), to deal in the Software without
//  restriction, including without limitation the rights to use,
//  copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following
//  conditions:
//  
//  The above copyright notice and this permission notice shall be
//  included in all copies or substantial portions of the Software.
//  
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//  OTHER DEALINGS IN THE SOFTWARE.


package goosegame

import scala.annotation.tailrec


trait DiceGame {
	def addPlayer(name: String): DiceGame
	
	def move(playerName: String): DiceGame = {
	  val random = new scala.util.Random(System.currentTimeMillis)
	  move(playerName,random.nextInt(6)+1,random.nextInt(6)+1)
	}
	
	def move(implicit playerName: String,firstDice: Int, secondDice: Int): DiceGame
	
	def msg(): List[String]
	
	def players(): Map[Player,Space]
	
	def getWinner(): Option[Player] = {
	  val playerSpace = players() find { case(p,space) => space == getLastSpace() }
		playerSpace map { case(p,space) => p}
	}

	def isEnded(): Boolean = {
	  getWinner().isDefined
	}

	def getLastSpace(): End
}


object GooseGame {

	def apply(players: Map[Player,Space] = Map[Player,Space]()): DiceGame = {
      val track =  ClassicTrack("classic")
		  	require(arePlayersInsideTrack(track,players), "the players must be on track")
			new GooseGame(track,players,List(),players.keySet)
	
	}

	def arePlayersInsideTrack(track: Track,players: Map[Player,Space]): Boolean = {
			players.forall { case(player,space) => track.contains(space)}
	}


	private class GooseGame  (track: Track, val players: Map[Player,Space] = Map[Player,Space](), val msg: List[String] =  List() , val enabledToMove : Set[Player] = Set() ) extends DiceGame{


		def addPlayer(name: String): GooseGame = {

				val playerExist = players.contains(Player(name))
				val allPlayerInStart = this.allPlayerInStart()

				(playerExist,allPlayerInStart) match {
					case (true,_) =>	new GooseGame(track, players, name+": already existing player" :: Nil,enabledToMove )
					case (false,false) => new GooseGame(track, players, "game is already started" :: Nil,enabledToMove )
					case (false,true) => {
						val newPlayers = players + ( Player(name) -> track.getStart)
						new GooseGame(track, newPlayers , List("Players: "+newPlayers.keys.map(_.name).mkString(", ")), newPlayers.keySet)
					}
				}

		}
	
		def move(implicit name: String, firstDice: Int, secondDice: Int): GooseGame = {
				val rollMsg = s"$name rolls $firstDice, $secondDice"
				val player = Player(name)
				val playerExistsAndCanMove = players.contains(player) && enabledToMove.contains(player)
				val gameEndend = isEnded()
				(playerExistsAndCanMove,gameEndend) match {
				  case (_,true) => 	new GooseGame(track, players, "Game ended" :: Nil )
				  case (false,_) => new GooseGame(track, players, name+": not existing player or cannot move" :: Nil,enabledToMove )
				  case (true,false) => {
						val initialSpace = players(player)
						val resultingSpaceWithActions = moveInternal(initialSpace, firstDice+secondDice, List(rollMsg))
             val updatedPlayersPosition = players.updated(player, resultingSpaceWithActions._1)
            
             val remaingPlayer = (enabledToMove - player)
             val newEnabledToMove = if(remaingPlayer.isEmpty) players.keySet else remaingPlayer
             
             //prank step
					  val x = players.find { case(p,space) => p != player && space == resultingSpaceWithActions._1 }
             x match {
						  case Some(playerSpace) =>{
						    val updatePlayer = updatedPlayersPosition.updated(x.get._1, initialSpace)
								val msg = "On "+resultingSpaceWithActions._1+" there is "+x.get._1.name+", who returns to "+initialSpace
								new GooseGame(track, updatePlayer , resultingSpaceWithActions._2 :+ msg, newEnabledToMove  )
							 }
						  case None => new GooseGame(track, updatedPlayersPosition , resultingSpaceWithActions._2,newEnabledToMove )
						}
				}
			}

		}
				

		def getLastSpace(): End = {
				track.getLastSpace
		}

		private def allPlayerInStart(): Boolean = {
				players.values.forall(_ == Start())
		}

		@tailrec
		private def moveInternal(initialSpace: Space,jump: Int, actions: List[String])(implicit playerName: String): (Space, List[String]) ={
				val eventualPosition = jump + initialSpace.position
				val lastPosition = getLastSpace().position
				val (newPosition,bounce) = if (eventualPosition <= lastPosition) (eventualPosition,false) else (lastPosition - (eventualPosition - lastPosition),true)
			  val newSpace = track.getSpace(newPosition)

				(initialSpace,newSpace,bounce) match {
						case (TheBridge(_,_),Regular(_),_) => (newSpace,actions :+ playerName+" jumps to "+newSpace)
				    case (TheGoose(_),Regular(_),_) => (newSpace, actions :+ playerName+" moves again and goes to "+newSpace)	
						case (_ ,Regular(_),false) => 	(newSpace, actions :+ playerName+" moves from "+initialSpace+" to "+newSpace)
						case (_ ,Regular(_),true) => 	(newSpace, actions :+ playerName+" moves from "+initialSpace+" to "+lastPosition+". "+playerName+" bounces! "+playerName+" returns to "+newSpace  )
						case ( _ ,End(_) ,_) => (newSpace, actions :+playerName+" moves from "+initialSpace+" to "+newSpace +". "+playerName+" wins!!")
						case (_ ,TheBridge(_,bridgePosition),_) => moveInternal( newSpace, bridgePosition, actions :+ playerName+" moves from "+initialSpace+" to The Bridge" )
						case (TheGoose(_),TheGoose(_),_) => moveInternal( newSpace, jump, actions :+ playerName+" moves again and goes to "+newSpace.position+" The Goose")
						case (_ ,TheGoose(_),_) => moveInternal( newSpace, jump, actions :+ playerName+" moves from "+initialSpace+" to "+newSpace.position+" The Goose")
						case (TheGoose(_),_,_) => moveInternal(newSpace, jump, actions :+ playerName+" moves again and goes to "+newSpace)
				}


		}
	}
}