//  Copyright (c) 2018 Damiano Fontana
//
//  Permission is hereby granted, free of charge, to any person
//  obtaining a copy of this software and associated documentation
//  files (the "Software"), to deal in the Software without
//  restriction, including without limitation the rights to use,
//  copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following
//  conditions:
//  
//  The above copyright notice and this permission notice shall be
//  included in all copies or substantial portions of the Software.
//  
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//  OTHER DEALINGS IN THE SOFTWARE.


package goosegame

import scala.annotation.tailrec

object Main extends App {
  
  println("The Goose Game")
     
  val game = GameFactory("goosegame")
  
  val history = gameInteraction(game, List(),0, (x) => scala.io.StdIn.readLine(), (str) => println(str))
  
  println("The Goose Game -> "+"Game Ended")

  
  @tailrec
  def gameInteraction(game: DiceGame, history: List[DiceGame],interactionCount: Int,readInput: (Int) => String, writeOutput: (String) => Unit ) : List[DiceGame] = {
    val addPlayerPattern = "add player\\s*([^\\n\\r]+)".r
    val dicePattern = "move\\s*([^\n\r]*) (\\d+)\\s*,\\s*(\\d+)".r
    val diceRandomPattern = "move\\s*([^\n\r]+)".r
    
       
    val userInput = readInput(interactionCount)
    userInput match{
      case addPlayerPattern(playerName) => {
    	    val newGame =  game addPlayer playerName 
    	    writeOutput("The Goose Game -> "+newGame.msg.mkString(". "))
    	    gameInteraction(newGame, newGame :: history,interactionCount+1,readInput,writeOutput)
    	  }
  
      case dicePattern(playerName,firstDice,secondDice) =>{ 
    	     val newGame =  game.move(playerName, firstDice.toInt, secondDice.toInt) 
    	     writeOutput("The Goose Game -> "+newGame.msg.mkString(". "))
    	     if(newGame.isEnded())
    	       newGame :: history
    	     else
    	       gameInteraction(newGame, newGame :: history,interactionCount+1,readInput,writeOutput)
    	  }
      case diceRandomPattern(playerName) =>{
    	    val newGame = game.move(playerName)
    	    writeOutput("The Goose Game -> "+newGame.msg.mkString(". "))
    	    if(newGame.isEnded())
    	       newGame :: history
    	     else
    	       gameInteraction(newGame, newGame :: history,interactionCount+1,readInput,writeOutput)
    	   }
      
    	  case "Exit" => history
    	  case _ => {
    	    writeOutput("The Goose Game -> unkwnow command. Choose one from {\"add player <playerName>\",\"move player <playerName> <dice1>, <dice2>\", \"move player <playerName>\" , \"Exit\"}")
    	    gameInteraction(game,history,interactionCount+1,readInput,writeOutput)
    	  }
    } 
     
  }
 
  
}