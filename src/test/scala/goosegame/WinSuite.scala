//  Copyright (c) 2018 Damiano Fontana
//
//  Permission is hereby granted, free of charge, to any person
//  obtaining a copy of this software and associated documentation
//  files (the "Software"), to deal in the Software without
//  restriction, including without limitation the rights to use,
//  copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following
//  conditions:
//  
//  The above copyright notice and this permission notice shall be
//  included in all copies or substantial portions of the Software.
//  
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//  OTHER DEALINGS IN THE SOFTWARE.


package goosegame

class WinSuite extends GameSuite {
  
  
  test("Victory"){
     val gameSteps = runGameSimulation(List("move Pippo 1,2"), Map[Player,Space](Player("Pippo") -> Regular(60)) )
     assert(gameSteps.size === 1)
     assert(gameSteps.head.players().get(Player("Pippo")) === Some(End(63)) )
     assert(gameSteps.head.msg().mkString(". ") ===    "Pippo rolls 1, 2. Pippo moves from 60 to 63. Pippo wins!!")
  }
  
  test("Winning with the exact dice shooting"){
    val gameSteps = runGameSimulation(List("move Pippo 3,2"), Map[Player,Space](Player("Pippo") -> Regular(60)) )
     assert(gameSteps.size === 1)
     assert(gameSteps.head.players().get(Player("Pippo")) === Some(Regular(61)) )
     assert(gameSteps.head.msg().mkString(". ") ===  "Pippo rolls 3, 2. Pippo moves from 60 to 63. Pippo bounces! Pippo returns to 61")
  }
}