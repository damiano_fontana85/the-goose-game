//  Copyright (c) 2018 Damiano Fontana
//
//  Permission is hereby granted, free of charge, to any person
//  obtaining a copy of this software and associated documentation
//  files (the "Software"), to deal in the Software without
//  restriction, including without limitation the rights to use,
//  copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following
//  conditions:
//  
//  The above copyright notice and this permission notice shall be
//  included in all copies or substantial portions of the Software.
//  
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//  OTHER DEALINGS IN THE SOFTWARE.


package goosegame

class MovePlayerSuite extends GameSuite {

  test("Start"){
     val gameSteps = runGameSimulation(List("add player Pippo","add player Pluto","move Pippo 1, 2","move Pluto 2, 2","move Pippo 2,5"))
     assert(gameSteps.size === 5)
     assert(gameSteps.head.players().keySet.map(_.name) === Set("Pippo","Pluto"))
     assert(gameSteps.head.players().get(Player("Pippo")) === Some(Regular(10)) )
     assert(gameSteps.head.players().get(Player("Pluto")) === Some(Regular(4)) ) 
     assert(gameSteps.head.msg().mkString(". ") === "Pippo rolls 2, 5. Pippo moves from 3 to 10")
     
     assert(gameSteps(1).msg().mkString(". ") === "Pluto rolls 2, 2. Pluto moves from Start to 4")
     assert(gameSteps(1).players().get(Player("Pluto")) === Some(Regular(4)))
    
     assert(gameSteps(2).msg().mkString(". ") === "Pippo rolls 1, 2. Pippo moves from Start to 3")
     assert(gameSteps(1).players().get(Player("Pippo")) === Some(Regular(3)))
    
  }
  
  
  test("Player cannot move two times"){
     val gameSteps = runGameSimulation(List("add player Pippo","add player Pluto","move Pippo 1, 2","move Pippo 2, 2","move Pluto 2,5"))
     assert(gameSteps.size === 5)
     assert(gameSteps.head.players().keySet.map(_.name) === Set("Pippo","Pluto"))
     assert(gameSteps.head.players().get(Player("Pippo")) === Some(Regular(3)) )
     assert(gameSteps.head.players().get(Player("Pluto")) === Some(Regular(7)) ) 
     assert(gameSteps.head.msg().mkString(". ") === "Pluto rolls 2, 5. Pluto moves from Start to 7")
     
     
  }
  
}